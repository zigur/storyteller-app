import { ref, onMounted, watch } from 'vue';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';
import { createWorker, OEM, PSM } from 'tesseract.js';

const PHOTO_STORAGE = "storyPictures";

export interface StoryPicture {
  filepath: string;
  webviewPath?: string;
  text?: string;
}

const worker = createWorker({
  logger: (msg: string) => console.log(msg),
  errorHandler: (err: Error) => console.error(err)
})

export function usePictureGallery() {

  const storyPictures = ref<StoryPicture[]>([]);

  const convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  const extractText = async(image: string): Promise<string> => {
    await worker.load();
    await worker.loadLanguage("eng");
    await worker.initialize("eng", OEM.LSTM_ONLY);
    await worker.setParameters({
      "tessedit_pageseg_mode": PSM.SINGLE_BLOCK
    });
    const { data } = await worker.detect(image);
    console.log(data);
    const { data: { text } } = await worker.recognize(image);
    await worker.terminate();
    return text;
  }

  const savePicture = async(photo: Photo, fileName: string): Promise<StoryPicture | undefined> => {
    if (!photo.webPath) {
      return;
    }
    const blob = await (await fetch(photo.webPath)).blob();
    const base64Data = await convertBlobToBase64(blob) as string;

    await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });

    const picture: StoryPicture = {
      filepath: fileName,
      webviewPath: photo.webPath
    };


    try {
      picture.text = await extractText(base64Data);
    } catch (err) {
      console.log(`Could not extract text: ${err}`);
    }

    return picture;
  };

  const cachePictures: () => void = () => {
    Storage.set({
      key: PHOTO_STORAGE,
      value: JSON.stringify(storyPictures.value)
    })
  }

  const loadSavedPictures = async() => {
    const storyPictureList = await Storage.get({ key: PHOTO_STORAGE });
    const picturesInStorage: StoryPicture[] = storyPictureList.value ? JSON.parse(storyPictureList.value) : [];

    for (const picture of picturesInStorage) {
      const file = await Filesystem.readFile({
        path: picture.filepath,
        directory: Directory.Data
      });
      picture.webviewPath = `data:image/jpeg;base64,${file.data}`;
    }

    storyPictures.value = picturesInStorage;

  }

  const takePicture: () =>  Promise<void> = async() => {
    const picture = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });
    const fileName = `${new Date().getTime()}.jpeg`;
    const savedFileImage = await savePicture(picture, fileName);
    if (savedFileImage) {
      storyPictures.value = [savedFileImage, ...storyPictures.value];
    }
  };

  watch(storyPictures, cachePictures);
  onMounted(loadSavedPictures);

  return {
    loadSavedPictures,
    storyPictures,
    takePicture
  };

}