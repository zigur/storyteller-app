// vuex.d.ts
import { Store } from 'vuex'

declare module '@vue/runtime-core' {

  interface AsyncState<P> {
    isPending: boolean;
    remoteError: BaseHttpError | null;
    data: P;
    isIdle?: boolean;
  }

  // declare your own store states
  interface StoryState {
    authors: AsyncState<Author[]>;
    books: AsyncState<Book[]>;
    stories: AsyncState<Story[]>;
    currentAuthor: AsyncState<Author | null>;
    currentBook: AsyncState<Book | null>;
    currentStory: AsyncState<Story | null>;
  }

  interface State {
    story: StoryState;
  }

  // provide typings for `this.$store`
  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}