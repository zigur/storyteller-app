import axios, { AxiosResponse } from "axios";
import { parseParams } from "./utils";

/*
export class HttpError {
  message: string;

  request: {};

  response: AxiosResponse<any, any> | undefined;

  constructor(
    message: string, 
    request: {}, 
    response: AxiosResponse<any, any> | undefined
  ) {
    this.message = message;
    this.request = request;
    this.response = response;
  }
} */


export const getAuthors = (params?: any): Promise<AxiosResponse<Author[], any>> => {
  const opts = params && { 
    params,
    paramsSerializer: (params: any) => parseParams(params) 
  };
  return axios.get("/api/authors", opts);
}

export const getAuthorById = (id: number): Promise<AxiosResponse<Author, any>> => {
  return axios.get(`/api/authors/${id}`);
}


export const getBooks = (params?: any): Promise<AxiosResponse<Book[], any>> => {
  const opts = params && { 
    params,
    paramsSerializer: (params: any) => parseParams(params) 
  };
  return axios.get("/api/books", opts);
}

export const getBookById = (id: number): Promise<AxiosResponse<Book, any>> => {
  return axios.get(`/api/books/${id}`);
}


export const getStories = (params?: any): Promise<AxiosResponse<Story[], any>> => {
  const opts = params && { 
    params,
    paramsSerializer: (params: any) => parseParams(params) 
  };
  return axios.get("/api/stories", opts);
}

export const getStoryById = (id: number): Promise<AxiosResponse<Story, any>> => {
  return axios.get(`/api/stories/${id}`);
}