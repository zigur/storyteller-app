import { Ref, ref } from "@vue/reactivity";
import { AxiosResponse } from "axios";

type FetcherData = Story | Story[] | null;

type FetcherError = BaseHttpError | null;

type FetcherFunc = (params?: any) => Promise<AxiosResponse>

export function useFetcher(fetcher: FetcherFunc) {
  const data: Ref<FetcherData> = ref(null);
  const loading: Ref<boolean> = ref(false);
  const error: Ref<FetcherError> = ref(null);
  const getData = async(...params: any) => {
    loading.value = true;
    data.value = null;
    error.value = null;
    try {
      const res = await fetcher(...params);
      data.value = res.data;
    } catch (err) {
      error.value = {
        message: "Oops",
        statusCode: 500,
      }
    } finally {
      loading.value = false;
    }    
  };

  return {
    data, loading, error, getData
  }
}