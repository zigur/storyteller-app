import { createRouter, createWebHistory } from '@ionic/vue-router';
import { RouteRecordRaw } from 'vue-router';
import Tabs from '../views/Tabs.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/tabs/stories'
  },
  {
    path: '/tabs/',
    component: Tabs,
    children: [
      {
        path: '',
        redirect: '/tabs/stories'
      },
      {
        path: 'capture',
        component: () => import('@/views/CaptureTab.vue')
      },
      {
        path: 'stories',
        component: () => import('@/views/StoriesTab.vue')
      },
      {
        path: 'authors',
        component: () => import('@/views/AuthorsTab.vue')
      },
      {
        path: 'books',
        component: () => import('@/views/BooksTab.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
