interface Author {
  id: number;
  fullName: string;
  biography?: string;
  books?: Books[];
  stories?: Story[];
}

interface Book {
  id: number;
  title: string;
  authors: Author[];
}

interface Story {
  id: number;
  text: string;
  book?: Book;
  author?: Author;
}

interface BaseHttpError {
  statusCode: number;
  message: string;
  _internal?: unknown;
}