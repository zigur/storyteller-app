import { getAuthorById, getAuthors, getBookById, getBooks, getStories, getStoryById } from "@/api/storytellerApi";
import { AxiosError } from "axios";
import { State, StoryState } from "vue";
import { ActionContext, ActionTree, Commit, GetterTree, Module, MutationTree } from "vuex";
import { StoryMutationTypes } from "./mutation-types";
import { StoryActionTypes } from "./action-names";

export const state: StoryState = {
  authors: {
    data: [],
    isPending: false,
    remoteError: null,
  },
  books: {
    data: [],
    isPending: false,
    remoteError: null,
  },
  stories: {
    data: [],
    isPending: false,
    remoteError: null,
  },
  currentAuthor: {
    data: null,
    isPending: false,
    remoteError: null,
  },
  currentBook: {
    data: null,
    isPending: false,
    remoteError: null,
  },
  currentStory: {
    data: null,
    isPending: false,
    remoteError: null,
  },
}


export const getters: GetterTree<StoryState, any> = {
  authorsForDisplay: (state) => {
    return state.authors.data.map(author => {
      return {
        fullName: author.fullName,
        biography: author.biography,
        bookCount: (author.books && author.books.length) ?? 0,
        storyCount: (author.stories && author.stories.length) ?? 0
      };
    });
  }
};


export const mutations: MutationTree<StoryState> = {
  [StoryMutationTypes.SET_AUTHORS](state: StoryState, { records }: { records: Author[] }) {
    state.authors.isPending = false;
    state.authors.data = records;
  },
  [StoryMutationTypes.SET_BOOKS](state: StoryState, { records }: { records: Book[] }) {
    state.books.isPending = false;
    state.books.data = records;
  },
  [StoryMutationTypes.SET_STORIES](state: StoryState, { records }: { records: Story[] }) {
    state.stories.isPending = false;
    state.stories.data = records;
  },
  [StoryMutationTypes.SET_STATUS_PENDING](state: StoryState, { target }: { target: keyof StoryState }) {
    state[target].isPending = true;
    state[target].remoteError = null;
  },
  [StoryMutationTypes.SET_STATUS_ERROR](state: StoryState, { error, target }: 
  { 
    error: BaseHttpError;
    target: keyof StoryState;
  }) {
    state[target].isPending = false;
    state[target].remoteError = error;    
  },
  [StoryMutationTypes.SET_CURRENT_AUTHOR](state: StoryState, { record }: { record: Author }) {
    state.currentAuthor.isPending = false;
    state.currentAuthor.data = record;
  },
  [StoryMutationTypes.SET_CURRENT_BOOK](state: StoryState, { record }: { record: Book }) {
    state.currentBook.isPending = false;
    state.currentBook.data = record;
  },
  [StoryMutationTypes.SET_CURRENT_STORY](state: StoryState, { record }: { record: Story }) {
    state.currentStory.isPending = false;
    state.currentStory.data = record;
  }
}

// TODO Actions
// type AugmentedActionContext = {
//  commit<K extends keyof Mutations>(key: K, payload: Parameters<Mutations[K]>[1]): ReturnType<Mutations[K]>;
// } & Omit<ActionContext<StoryState, State>, 'commit'>

export interface Actions {
  [StoryActionTypes.ACTION_GET_STORIES](
    { commit }: ActionContext<StoryState, State>,
    { params }: { params: any }
  ): Promise<void>;
  [StoryActionTypes.ACTION_GET_STORY](
    { commit }: ActionContext<StoryState, State>,
    { id }: { id: number }
  ): Promise<void>;
}

const handleError = (commit: Commit, err: AxiosError, target: keyof StoryState) => {
  const error: BaseHttpError = {
    message: err.code || "Remote Error",
    statusCode: (err.response && err.response.status) || 500,
    _internal: err
  }
  commit(StoryMutationTypes.SET_STATUS_ERROR, { error, target });
}

export const actions: ActionTree<StoryState, State> & Actions = {
  async [StoryActionTypes.ACTION_GET_AUTHORS](
    { commit, state }: ActionContext<StoryState, State>,
    { params }: { params: any }
  ) {
    try {
      commit(StoryMutationTypes.SET_STATUS_PENDING, { target: "authors" });
      const { data } = await getAuthors(params);
      commit(StoryMutationTypes.SET_AUTHORS, { records: data });
    } catch (err) {
      handleError(commit, err as AxiosError, "authors");
    }
  },
  async [StoryActionTypes.ACTION_GET_AUTHOR](
    { commit, state }: ActionContext<StoryState, State>,
    { id }: { id: number }
  ) {
    try {
      commit(StoryMutationTypes.SET_STATUS_PENDING, { target: "currentAuthor" });
      const { data } = await getAuthorById(id);
      commit(StoryMutationTypes.SET_CURRENT_AUTHOR, { record: data });
    } catch (err) {
      handleError(commit, err as AxiosError, "currentAuthor");
    }
  },
  async [StoryActionTypes.ACTION_GET_BOOKS](
    { commit, state }: ActionContext<StoryState, State>,
    { params }: { params: any }
  ) {
    try {
      commit(StoryMutationTypes.SET_STATUS_PENDING, { target: "books" });
      const { data } = await getBooks(params);
      commit(StoryMutationTypes.SET_BOOKS, { records: data });
    } catch (err) {
      handleError(commit, err as AxiosError, "books");
    }
  },
  async [StoryActionTypes.ACTION_GET_BOOK](
    { commit, state }: ActionContext<StoryState, State>,
    { id }: { id: number }
  ) {
    try {
      commit(StoryMutationTypes.SET_STATUS_PENDING, { target: "currentBook" });
      const { data } = await getBookById(id);
      commit(StoryMutationTypes.SET_CURRENT_BOOK, { record: data });
    } catch (err) {
      handleError(commit, err as AxiosError, "currentBook");
    }
  },
  async [StoryActionTypes.ACTION_GET_STORIES](
    { commit, state }: ActionContext<StoryState, State>,
    { params }: { params: any }
  ) {
    try {
      commit(StoryMutationTypes.SET_STATUS_PENDING, { target: "stories" });
      const { data } = await getStories(params);
      commit(StoryMutationTypes.SET_STORIES, { records: data });
    } catch (err) {
      handleError(commit, err as AxiosError, "stories");
    }
  },
  async [StoryActionTypes.ACTION_GET_STORY](
    { commit, state }: ActionContext<StoryState, State>,
    { id }: { id: number }
  ) {
    try {
      commit(StoryMutationTypes.SET_STATUS_PENDING, { target: "currentStory" });
      const { data } = await getStoryById(id);
      commit(StoryMutationTypes.SET_CURRENT_STORY, { record: data });
    } catch (err) {
      handleError(commit, err as AxiosError, "currentStory");
    }
  }
}

const store: Module<StoryState, State> = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

export default store;