import { InjectionKey } from '@vue/runtime-core';
import { State } from 'vue';
import { createStore, useStore as baseUseStore, Store, createLogger } from 'vuex'
import storyStore from '@/store/story';

// Plug in logger when in development environment
const debug = process.env.NODE_ENV !== 'production'
const plugins = debug ? [createLogger({})] : []
// Plug in session storage based persistence
// plugins.push(createPersistedState({ storage: window.sessionStorage }))

// define injection key
export const key: InjectionKey<Store<State>> = Symbol();

// Create a new store instance.
export const store = createStore<State>({
  modules: {
    story: storyStore,
  },
  plugins
});

// define your own `useStore` composition function
export function useStore() {
    return baseUseStore(key);
}
