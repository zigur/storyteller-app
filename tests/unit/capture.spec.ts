import { mount } from '@vue/test-utils'
import CaptureTab from '@/views/CaptureTab.vue'

describe('CaptureTab.vue', () => {
  it('renders tab 1 view', () => {
    const wrapper = mount(CaptureTab)
    expect(wrapper.text()).toMatch('CaptureTab')
  })
})
