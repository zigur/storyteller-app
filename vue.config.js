module.exports = {
  runtimeCompiler: true,      
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3003/',
        changeOrigin: true,
        logLevel: 'info',
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'assets'
}